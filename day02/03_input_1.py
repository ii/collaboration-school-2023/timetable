print("God morgen!")

# Hvorfor setter vi et mellomrom etter spørsmålstegn?
abcd = input("Hva heter du? ")

# Her gjør komma det samme som mellomrommet i forrige linje
print("Hyggelig å hilse på deg,", abcd)
print(abcd)
print(abcd * 10)