def my_sum(numbers):
    # Instansiere en variabel å bygge på.
    summe = 0

    # Kjør en for-løkke som bygger opp verdien i variabelen.
    for number in numbers:
        summe += number

    return summe


# Beregner minimum av en liste med negative tall.
def my_min(numbers):
    # Instansiere en variabel å bygge på.
    minimum = numbers[0]

    # Kjør en for-løkke som bygger opp verdien i variabelen.
    for number in numbers:
        if number < minimum:
            minimum = number

    return minimum


# Returnerer alle verdiene i 'xs' som er større enn 'lower_bound'.
def filter_greater_than(xs, lower_bound):
    # Instansiere en variabel å bygge på.
    filtered_list = []

    # Kjør en for-løkke som bygger opp verdien i variabelen.
    for number in xs:
        if number > lower_bound:
            filtered_list.append(number)

    return filtered_list


# Konstruerer et histogram fra verdiene i 'values'.
def render_histogram(values):
    # Instansiere en variabel å bygge på.
    rows = []

    # Kjør en for-løkke som bygger opp verdien i variabelen.
    for value in values:
        rows.append("*" * value)

    return "\n".join(rows)  # det neste eksempelet viser mer om .join()


print(my_sum([1, 2, 3, 4, 5]))
print(my_min([-1, -2, -8, -4, -5]))
print(filter_greater_than([1, 2, 3, 4, 5], 3))
print(render_histogram([1, 2, 3, 4, 5, 3, 8, 1]))
