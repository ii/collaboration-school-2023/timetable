x = [4, 6, 1, 8]

names = ['Ali', 'David']
numbers = [5551234, 5554321]



for i in x:
    print(i)

print('The 1-item is the number', x[1:3])



phonebook = { 'Ali' : 5551234, 'David' : 5554321 }

ali_num = phonebook['Ali']
print(ali_num)

print(phonebook.values())

####
discounts = { 
    'Alice' : ['student'], 
    'Bob' : ['senior'], 
    'Clare' : ['student', 'senior'], 
}

for name, disc in discounts.items():
    print(name, 'has the following discounts:')
    for d in disc:
        print(d)

