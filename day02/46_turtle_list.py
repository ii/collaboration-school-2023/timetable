import turtle as t

# list of angles, list of lengths
angles = [90.0, 90.0, 90.0, 90.0, 90.0]
lengths = [100, 120, 150, 180, 300]

# loop over each item in the two lists
if len(angles) != len(lengths):  # why have this here?
    print("lists not same length")
    exit() # stops the program

# we use this combined list to give instructions to the turtle to draw a square:
for i in range(len(angles)):
    t.left(angles[i])
    t.forward(lengths[i])

t.done()
