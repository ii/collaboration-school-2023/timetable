en_liste = ["a", "b", "c", "d", "e"]
print(f"{en_liste = }")

# Vi kommer åt verdien på indeks i ved å skrive en_liste[i].
print(f"{en_liste[0] = }")
print(f"{en_liste[1] = }")

# Siden indeks begynner på 0 så kommer det siste elementet ha indeks
# et mindre enn lengden av listen.
# print(en_liste[len(en_liste)])  # Dette gir en feilmelding.
print(f"{en_liste[4] = }")

# Indeks på siste elementet er alltid lengden av listen minus én.
print(f"{en_liste[len(en_liste) - 1] = }")

# Men vi kan også få det siste elementet ved å skrive kun [-1].
# Python subtraherer negative indeksverdier fra len() automatisk
print(f"{en_liste[-1] = }")

# Vi kan også endre verdiene i listen ved å bruke indeks.
print("Vi endrer listen ved å skrive: en_liste[2] = 'å'")
en_liste[2] = "å"
print(f"{en_liste = }")

# Vi kan få ut dellisten fra index i til og med j-1 ved å skrive [i:j].
print(f"{en_liste[1:4] = }")

# Vi kan dessuten hoppe over element.
print(f"{en_liste[0:5:2] = }")  # en_liste[start:stop:step]

# Vi gå andre veien i listen.
print(f"{en_liste[3:1:-1] = }")

# Vi må ikke angi start og/eller stop om vi vil gå helt fra start og/eller helt til slutten.
print(f"{en_liste[:3] = }")
print(f"{en_liste[3::-1] = }")
print(f"{en_liste[:] = }")

# Vi kan også fjerne verdiene i listen ved å bruke del.
print("Vi endrer listen ved å skrive: del en_liste[3]")
del en_liste[3]  # Tar bort elementet på indeks 3.
print(f"{en_liste = }")  # en_liste er endret.
