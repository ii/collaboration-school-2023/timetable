# https://docs.python.org/3/tutorial/datastructures.html

a = [5, 7, 2, 9, 10]
print(f"a = {a}")


# .append()

a.append(7)
print(f"We append 7: {a}")


# .insert()

a.insert(2, 6)
print(f"We insert 6 at index 2: {a}")


# .reverse()

a.reverse()
print(f"We reverse a: {a}")


# .remove()

a.remove(7)
print(f"We remove the first occurence of 7: {a}")


# .sort()

a.sort()
print(f"We sort a: {a}")
