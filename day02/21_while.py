print("Countdown...")
num = 10  # start
while num >= 0:  # test

    num -= 1  # update
    print(num)

    
exit()

# first square less than 1000
n = 1  # start
while n * n < 1000:  # test
    n += 1  # update
print(n, "is the first number where n^2 is larger than 1000")

# sleepy triangle
s = ""  # start
while s != "ZZZZZZZZZZZ":  # test
    print(s)
    s += "Z"  # update
