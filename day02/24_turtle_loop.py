import turtle as t

t.speed(0)
t.colormode(255)  # set color to rgb color mode (r,g,b)

sidelength = 20  # set length of side

for i in range(30):
    # update position and color
    t.right(15)
    t.color(255 - (3 * i), 132 - (3 * i), 190 - (3 * i))

    # draw basic square
    sides = 0
    while sides < 4:
        t.forward(sidelength + (5 * i))
        t.right(90)
        sides += 1

t.done()
