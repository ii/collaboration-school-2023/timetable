a = [5, 7, 2, 9, 10]

# You can use 'len()' with lists
print(a, len(a))

# You can use 'if _ in _:' with lists
if 9 in a:
    print(f"9 is in the list a = {a}!")

b = [5, "str", 9.3]
print("Lists can have mixed types inside:", b)

c = a + b
print(f"We can concatenate lists: {c = }")

# You can extract values from a list
x, y, z = [1, 5, 7]
print(x)
print(y)
print(z)
