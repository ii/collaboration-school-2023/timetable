print("----------------------")
# Function that takes no input and always returns the same value


def always_12(): # funksjonsdefinisjon
    return 12 # returnverdien


print("always_12 function:")
print(always_12()) # funksjonskall (= bruk av funksjonen)
x = always_12() + 100  # funksjonskall blir erstattet med returnverdien
print(x)



print("----------------------")


# What if we want to add 2 to a given number?
print(4 + 2)
print(7 + 2)
print(1 + 2)

# We can do this easily with a function instead, with a single input argument (our given number)
def add_2(num):   # ubestemt funksjonsargument "num", blir fylt ut når vi bruker funksjonen
    return num + 2 # kan bruke num inni funksjonen, men er ikke definert utenfor


print("add_2 function:")
print(add_2(4))  # num blir sett til 4, og så erstattes funksjonskall med return-verdi
print(100 + add_2(7) + 900) # num blir sett til 7 og så erstattes funksjonskall med return-verdi


print("----------------------")


# multi-input function with only integers
def add_multiply(add, mult):
    return (2 + add) * mult


print("add_multiply function:")
print(add_multiply(2, 4))
n = 5
print(add_multiply(8, n))
print(add_multiply(n, n+n)) # funksjonsargumenter kan være hva som helt uttrykk



print("----------------------")

# multi-input function with mixed type inputs
def mixed(word, num, punct):
    result = f"{word}{punct} " * num
    return result


print("mixed type input function:")
print(mixed("Hello", 2, "!"))
print(mixed("What", 5, "?"))
