# Use a counting loop to do something 7 times.
n = 0
while n < 7:
    print("Hello!")
    n += 1


# Read input 5 times:
n = 0
while n < 5:
    name = input(f"Name {n+1}? ")
    print("Hello,", name)
    n += 1


# Running an endless loop with a break condition is a common pattern.

while True:
    answer = input("Should we continue? ")
    if answer == "n":
        break
    print("Great, let's go one more time...")

print("Bye!")


##################################################
# "continue" can be used to jump over something. What does the code below jump over?

# If you are confused about what this code does you can include
# more print statements.
for i in range(100):
    if "4" in str(i):
        continue
    print(i)
