import turtle as t

t.shape("turtle")  # hva skjer hvis vi kommenterer ut denne linjen?

# prøv å endre rekkefølgen på instruksjonene
t.forward(100)
t.left(100)
t.forward(70)
t.right(100)
t.forward(100)
t.left(30)
t.forward(50)

t.done()  # hva skjer hvis vi kommenterer ut denne linjen?
