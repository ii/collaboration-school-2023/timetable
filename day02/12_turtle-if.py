import turtle as t

t.forward(100)

response_1 = input(
    "Direction 1 (enter 'l' to turn left, or 'r' to turn right.)? "
)
if response_1 == "r":
    t.right(90)
else:
    t.left(90)

###
t.forward(100)

response_2 = input(
    "Direction 2 (enter 'l' to turn left, or 'r' to turn right.)? "
)
if response_2 == "r":
    t.right(90)
else:
    t.left(90)

###
t.forward(100)

response_3 = input(
    "Direction 3 (enter 'l' to turn left, or 'r' to turn right.)? "
)
if response_3 == "r":
    t.right(90)
else:
    t.left(90)

###
t.forward(100)


t.done()
