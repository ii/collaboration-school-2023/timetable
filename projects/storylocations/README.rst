Story locations
===============

Your task is to plot the place names mentioned in a given novel on a geographical map. Feeding in "Around the World in 80 days" should result in a plot of the journey across the globe.

Source texts are available e.g. at https://www.gutenberg.org/

A source of world city names and coordinates is https://download.geonames.org/export/dump/, for example the cities...zip series, including all cities with a population above a certain threshold.

You can consider named entity recognition (see links below), but it may be easier at first to match all words in the text to all known city names using something like https://github.com/vi3k6i5/flashtext/.

Start with a simple command line tool, and build on from there. You could include 
* user interaction, together with automatic download of the chosen work from gutenberg.org
* a web interface, using https://streamlit.io/ 
* a more robust analysis looking at surrounding text, to try to distinguish the location of the plot from placenames that are just mentioned by a character
* anything else you can think of

+++++++++++++++
Other resources
+++++++++++++++
* Named entity recognition (NER) using the Python library NLTK: https://machinelearningknowledge.ai/beginners-guide-to-named-entity-recognition-ner-in-nltk-library-python/?utm_content=cmp-true 
* REST APIs for Norwegian NER: https://ai.nb.no/2022/01/04/named-entity-recognition-ner/ 
* Using a REST API in python: https://www.geeksforgeeks.org/response-json-python-requests/
* REST API that searches for coordinates: https://nominatim.org/release-docs/develop/api/Search/
    example query: https://nominatim.openstreetmap.org/search.php?city=Bergen&country=Norway&format=jsonv2






