# Folktales of the World

A motif is any distinctive feature idea that helps to develop the mood or theme of a story, for instance, *transformation*. The [Thompson Motif Index](https://ia800408.us.archive.org/30/items/Thompson2016MotifIndex/Thompson_2016_Motif-Index.pdf) is a six-volume catalogue of motifs used in folktales from across the world. 

For this project, your task is to use visualization to help you to analyze these folktale motifs: where did these various motifs originate, how do they relate, and/or how often do certain words, beings, or phrases recur across different motifs, e.g., *fairies*? 

Consider your questions, and then choose a visualization type most suited to answering your question, as we discussed in the visualization module in the previous week. For instance, to understand the connections between and across these different motifs, a network diagram may be the most useful choice, with some example code for generating a network in the file `./network-example.py`. To identify the source of these motifs, you can consider a chloropleth map. 

Take the CSV file in the directory `./TMI_as_CSV` as a starting point, which has already assembled and done some preparation steps to the data. Generate at least three different visualizations of the dataset to help you better understand this dataset, and choose one of these visualizations to refine in a "production-ready"-style image with clear titles, labels, color choices, etc. 
