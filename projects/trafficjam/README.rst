Traffic Jam
===========

In this project, you will create an agent-based simulation in Python to study emergent traffic behaviour. Object orientation is a possible design choice here, but not a must.

Task description
----------------

You can break most of these constraints if you have a good reason. The
only rule that you *must* stick to is: **Have something to show on Friday!**

Road
   Simulate a stretch of road, several kilometers long. At first with just a single lane, but keep an extension to multi-lane roads in mind.

Cars
   Onto this road you place cars that each have an autonomous behaviour, governed by parameters such as: desired target speed, acceleration, deceleration, desired distance to the car in front, etc. At first, the behaviour rules for all cars are the same.

Visualisation
   One view should be an animation showing the cars moving along the road. In addition, show a position-time graph with one line for each car plotted together on the same sheet. An example for anmations in matplotlib can be found `in this folder <bounce.py>`_

Initial task
------------
In a symmetric setup, with identical cars at identical speeds in equal distance from each other, everyone will move smoothly and uniformly at the selected speed.

If *one* of the cars slows down for a moment (not stopping!), and then comes back to speed, the behaviour of the system will depend on traffic density (cars/km). At low density, the disturbance affects very few cars, and the steady flow is achieved again quickly. But when the density is above a critical value, this small disturbance creates a cascading effect, causing a queue of cars that are forced to stop completely, while the original problem car drives on, unaware of the trouble it caused.

Try to observe this behaviour in your simulation.

Extensions
----------
* Multiple lanes and lane-switch decisions
* On-ramp: slower cars joining a fast-moving highway
* More car types: fast car/slow truck, aggressive/gentle drivers, ...

See also
--------
https://www.youtube.com/watch?v=Suugn-p5C1M
