import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

# Set up data storage. 100 points with x,y,vx,vy
state = np.random.rand(100,4)
state -= 0.5
state *= 2

# convenient shortcuts
pos = state[:,:2]
vel = state[:,2:]

vel *= 0.01

xs = state[:,0]
ys = state[:,1]

vxs = state[:,2]
vys = state[:,3]


# set up plot
fig, ax = plt.subplots()

ax.set_xlim(-1,1)
ax.set_ylim(-1,1)
ax.set_aspect('equal')

dots, = ax.plot(xs,ys,'.')

def timestep():
    global pos, vel
    pos += vel # *dt (assume dt = 1)
    # reflect off the walls
    vxs[(xs < -1) | (xs > 1)] *= -1
    vys[(ys < -1) | (ys > 1)] *= -1

def animate(frame_no):
    #print(frame_no)
    timestep()
    dots.set_data(xs,ys)
    return dots,

anim = FuncAnimation(fig,animate,
                     interval=20)

plt.show()
