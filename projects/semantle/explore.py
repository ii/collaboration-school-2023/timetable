# script version of 
# https://colab.research.google.com/drive/1Le52ism68viyx4n_U2Pf5UGYpP1DlFLv?usp=sharing by Koenraad de Smedt

# Word vector semantics with visualization
# Adapted from Chris Manning's class CS224n at Stanford.
# This notebook is a demonstration of how word vectors can be used for finding 
# semantically similar words.

# Gensim will be used. Gensim is a package for word and text similarity 
# modeling. It is efficient and scalable, and quite widely used. Gensim can use 
# GloVe vectors, as will be demonstrated.


import numpy as np

# Get the interactive Tools for Matplotlib
import matplotlib.pyplot as plt
# plt.style.use('ggplot')

from sklearn.decomposition import PCA

from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec

NUM_DIMS = 100
glove_file = f'glove.6B.{NUM_DIMS}d.txt'
word2vec_glove_file = f"glove.6B.{NUM_DIMS}d.word2vec.txt"

try: # try loading the word2vec file if it already exists
  model = KeyedVectors.load_word2vec_format(word2vec_glove_file)
except: # generate from glove file
  glove2word2vec(glove_file, word2vec_glove_file)
  model = KeyedVectors.load_word2vec_format(word2vec_glove_file)


print(model.similarity('man', 'boy'))

print(model.most_similar('man'))

print(model.most_similar('strawberry'))

#print(model.most_similar(negative='knitting'))

####

def analogy(x1, x2, y1):
  result = model.most_similar(positive=[y1, x2], negative=[x1])
  return result[0][0]


print(analogy('man', 'king', 'woman'))
print(analogy('japan', 'japanese', 'norway'))
print(analogy('australia', 'beer', 'france'))
print(analogy('tall', 'tallest', 'long'))
print(analogy('good', 'fantastic', 'bad'))
print(analogy('stereo', 'music', 'tv'))

# Unexpected things may happen, especially if words are ambiguous.
print(analogy('bottle', 'cork', 'pan'))

# Guess what the following does.
print(model.doesnt_match({'breakfast', 'cereal', 'dinner', 'lunch'}))




####

def display_pca_scatterplot(model, words=None):
  if words != None:
    # make array of vector in model for each word
    word_vectors = np.array([model[w] for w in set(words)])
    # transform to two-dimensional plane
    twodim = PCA().fit_transform(word_vectors)[:,:2]
    # show scatter plot
    plt.figure(figsize=(8,8))
    plt.scatter(twodim[:,0], twodim[:,1], edgecolors='k', c='r')
    for word, (x,y) in zip(words, twodim):
        plt.text(x+0.05, y+0.05, word)
    plt.show()

display_pca_scatterplot(model, 
                        {'coffee', 'tea', 'beer', 'wine', 'brandy', 'champagne', 'water',
                         'spaghetti', 'borscht', 'hamburger', 'pizza', 'sushi', 'meatballs',
                         'cat', 'reindeer', 'monkey', 'parrot', 'fish', 'koala', 'lizard',
                         'norway', 'germany', 'luxembourg', 'australia', 'fiji', 'china',
                         'homework', 'assignment', 'problem', 'exam', 'test', 'class',
                         'school', 'college', 'university', 'institute'})

