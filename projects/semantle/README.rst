Semantle
========

Your task is to implement a version of the game `Semantle <https://semantle.com/>`_. Start with a prototype that interacts on the command line with the simplest possible interactions, before expanding out from there.

The file `explore.py <explore.py>`_ contains the content of a `worksheet by Koenraad de Smedt <https://colab.research.google.com/drive/1Le52ism68viyx4n_U2Pf5UGYpP1DlFLv?usp=sharing>`_ on word embeddings, the word2vec format, and the gensim library which could be helpful. 

The GLOVE database is available at https://nlp.stanford.edu/projects/glove/. Look at the worksheet to see how similarity can be calculated, and implement the game's interface from there.

A  short intro to word embeddings is e.g. 
https://towardsdatascience.com/word2vec-explained-49c52b4ccb71


Possible extensions can be 

* showing a history of previous guesses like the actual Semantle does
* a web-based version using the https://streamlit.io/ library
* a scatterplot visualization, which plots the mystery word in relation with the user's previous guesses (see explore.py for an example)
* anything else you might want to do with the similarity vectors. Some more inspiration can be found on Koenraad's other worksheets at the end of his `course page <https://mitt.uib.no/courses/38115/pages/python-notebooks?module_item_id=358078>`_ .
