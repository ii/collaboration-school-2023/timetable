To reproduce the website in the Sphinx slides, 

1) follow the slides to install sphinx.
2) run 
```
sphinx-quickstart
```
in this directory.
3) copy the files in the "files/source" directory and replace the existing files in the source directory
4) run 
``` 
make html
```
5) open "index.html" located in the build directory.
