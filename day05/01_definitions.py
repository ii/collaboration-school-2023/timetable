# Function definition
# function name: shout
# function arguments / parameters: 
#      we have 1 of them : message
def shout(message):
    # function body
    result = message + '!'
    # return value: result
    return result

# assignment of the value "Hei" into the variable x
x = "Hei"
# function call, with x as the "message" parameter
# assignment of the function's return value into shouty_x
shouty_x = shout(x) # shout(x) -> "Hei!" 
# can also do functon call with named parameter
# shouty_x = shout(message=x) 
print(shouty_x)
