x = "Hei there how are you doing"

# found "endswith" in the python documentation
# let's try it
print(x.endswith("here"))

# optional arguments...
print(x.endswith("here", 0, 9))

# ...have the same effect as
substr = x[0:9]
print(substr.endswith("here"))
