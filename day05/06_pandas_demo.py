import pandas as pd

# bring in data
source = pd.read_csv("./gapminder.csv")
print(source.columns)
print(source.ndim)
print(source.shape)
num_rows, num_cols = source.shape
print(num_rows, num_cols)
middle = num_rows//2
print(source.loc[middle])
print('---')
print(source.loc[0])

print(source.dtypes)

print('*'*25)

print(source['Year'])
print('====')
print(source['Population'].mean())
print(source['Year'] == 2012)

# filter is an array of True/False
filt = (source['Year'] == 2012)
#filt = [True, False] * (39988//2)
source_2012 = source[filt]
print(source_2012)
print(source_2012.describe())
print("=-=-=-=-")
print(source["Region"].unique())

source["Region"] = source["Region"].str.capitalize()

source2014 = source.loc[source["Year"] == 2014]

print(source2014.head(12))

