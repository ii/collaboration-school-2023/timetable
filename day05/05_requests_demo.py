import requests

url = 'https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=60.3913&lon=5.3221'

hds = {
    # "User-Agent" is used to identfy the user. 
    # It can contain any text, but
    # some APIs have requirements on what to put in here.
    # This string will end up in the server's access log.
    'User-Agent': 'UiB_collabschool_2023 https://www.uib.no/en/digitallab/160176/collaborative-software-development-school',
}

r = requests.get(url, headers=hds)
print(r.text)
# Aha, it looks like json format 
# (reading the description on the source website would have told us that, too)

exit()

# Let's do some exploring...
import json # one of the standard libraries
weather = json.loads(r.text)

# 'weather' is now a python data structure with lists and dicts
# explore interactively

print(weather.keys())
exit()

print(weather['geometry'])
print(weather['properties'].keys())

# and so on...