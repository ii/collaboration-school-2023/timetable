Useful libraries
================

Python standard library
-----------------------
https://docs.python.org/3/library/

Documentation for all built-in data types as well as many 
useful libraries, have a look through to get an idea. 

Some suggestions: 

* re
* difflib 
* unicodedata
* readline
* datetime
* collections
* decimal, fractions
* random
* itertools
* pathlib
* shutil
* zipfile, tarfile
* csv, json, tomllib, configparser, html, xml
* time
* argparse
* subprocess (run other programs from within Python)
* http.server
* turtle :-)
* tkinter (easy graphical user interfaces; `try this tutorial <https://realpython.com/python-gui-tkinter/>`_)
* sys

External libraries
==================

Most are available at https://pypi.org/, can be installed with 
:code:`pip install ...` or our `install.py script <../day03/install.py>`_.

The search at PyPI doesn't help with distinguishing homework projects from serious tools. Better do a web search for something 
like ``python modules for astronomy``
and try out the popular looking ones.

Almost standard
---------------
The following libraries are worth looking at, 
since they form the basis for many other more specialised ones:

* `requests <https://requests.readthedocs.io/en/latest/>`_ - download data from URLs (see `05_requests_demo.py`_)
* https://numpy.org/ - The efficient numpy array data structure
* https://matplotlib.org/ - Easy plotting
* https://pandas.pydata.org/ - Python's own R
* https://www.nltk.org/ - Natural Language Toolkit; comes with their  detailed `NLTK book <https://www.nltk.org/book/>`_ teaching basic Python along with the library.


More specialist
---------------
Too many to list here, but these are on-topic for the school:

* https://lingpy.org/

