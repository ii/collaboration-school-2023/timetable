# optional arguments have a default value
def shout(message, exclamation_mark="!"):
    result = message + exclamation_mark
    return result

print(shout("Hei"))

print(shout("hei", "!!!!!@@@@@!!!!!"))

