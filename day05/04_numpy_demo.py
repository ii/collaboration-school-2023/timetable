import numpy as np
import matplotlib.pyplot as plt

# lists can mix data types
xs = [3, 5, 1, 9, 10, "Hei", "foo"]

# arays cannot
xs = np.array([2,6,1,9,6])
# allows for convenient operations on all elements
ys = xs * 100
print(ys + xs)
print(np.sin(ys))

xs = np.linspace(-10,10,300)
ys = 2 * xs**2 - xs + 2 + 100 * np.sin(xs)
plt.plot(xs,ys,'o')
plt.show()
