Useful Resources
================

Command line tools
------------------

* https://software-carpentry.org/lessons/
* https://ictp.grelli.org/bash/index.html David's older set of bash intro slides


Python basics
-------------

* https://docs.python.org/3/tutorial/ - suited for people who already have some experience of programming in general
* https://automatetheboringstuff.com/#toc - very nice basic into. The first half is the textbook for INF100 at UiB
* `Nick Montfort, Exploratory Programming for the Arts and Humanities, 2021. <books/Montfort__Exploratory_Programming.pdf>`_ CC-BY-NC-SA 

* http://pythontutor.com/visualize.html#mode=edit to visualize program flow



Python libraries
----------------

See the `library list under day05 <day05/README.rst>`_.

Python exercises
----------------

Small exercises focused on specific core features of Python:

* https://open.kattis.com/
* https://edabit.com/challenges/python3
* https://codingbat.com/python
* https://www.w3schools.com/python/

Larger, integrated problem solving exercises:

* https://projecteuler.net/archives - mathematical tasks
* https://adventofcode.com/events - Annual advent calendar with puzzles, very fun to solve



Other school-related links
--------------------------

* https://www.nb.no/dh-lab/
* https://clarino.uib.no/
* https://dhlab.hypotheses.org/tag/rest-api

* `Montfort et al., 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, 2013. <books/10_PRINT_121114.pdf>`_ `CC-BY-NC-SA-3.0 <https://creativecommons.org/licenses/by-nc-sa/3.0/>`_

* Julia Evans, https://wizardzines.com/ - Graphical explanations of many useful concepts. "How numbers work", "Debugging strategies", "Linux command line tools"

* `Falsehoods programmers believe about names <https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/>`_
* `A collection of similar falsehood lists <https://github.com/kdeldycke/awesome-falsehood>`_

