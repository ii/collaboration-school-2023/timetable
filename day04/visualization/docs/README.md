# Visualization
### Background and Python Tools 
**Laura Garrison**<br>
*Visualization Group, Institute for Informatics, University of Bergen*<br>
June 2023

## What this module covers:
- When to use visualization
- Basic types of visualizations, how they are different, and when it is best to use each type
- Optimal chart types for your data and questions you have about your data
- How to transform your data for visualization
- How to create visualizations using two popular Python charting libraries:" [`vega-altair`](https://altair-viz.github.io/) and [`matplotlib`](https://matplotlib.org/)
- How to add interactive features to your visualizations

## Setup 
- Run `$ pip install altair vega_datasets altair_viewer matplotlib` to install the Python libraries you need for today, which will include:
  - matplotlib
  - vega-altair
- All other packages that these depend on should be already in your system from cloning the git repository earlier this week. Alternatively, run `$ pip install requirements.txt` to make sure everything you need is in your system. 


## Visualization Background
### What is visualization? 
> "Computer-based visualization systems provide visual representations of datasets designed to help people carry out tasks more effectively."
*--Tamara Munzner, Visualization Analysis and Design, CRC Press 2014*

Essentially, a visualization:
- is based on data
- maps data to visual properties
- aims to help **you** or **your audience** SEE what is in the data and be able to do something about it. One of the keys to this whole process is bearing in mind who you are visualizing for and what they need to achieve with the visualization, as this will guide many of your decisions in choosing the "right" visualization. 

### Why visualize data? 
Visualization can be very useful to help us to quickly identify patterns, trends, outliers, etc. by leveraging our visual perceptual system. A classic use case for visualization is that of [Anscombe's Quartet (1973)](https://en.wikipedia.org/wiki/Anscombe%27s_quartet). This is a set of four datasets with shared statistical properties: mean, variance, and regression coefficient. Looking at these values, the datasets would appear to be the same--when visualized, they are clearly not:
![Anscombe's Quartet](./anscombe.png)

Visualization can also help us to manage what is known as the [Rashomon Effect]()--borrowed from psychology to describe the phenomenon of different people having different perceptions or memories of the same event, inspired from a 1950's Japanese film that tells the story of a Samurai's murder through four different perspectives. When we are analyzing data, different models, parameters, and representations all can tell different stories. Visualization can help us to spot and understand the reasons for these differences. 
![Rashomon Effect](./rashomon.png)

Visualization can help you navigate your data when your problem itself is fuzzy or poorly-defined: if you do not really know yet what you are looking for, how can you find it? Doing so through pure analytical tasks can quickly become tiresome, and visualization can facilitate a more open exploration of your data. 

Visualization can also help you to verify, and to build trust, in your analyses, and provide an accessible means of sharing results, whether final or in progress. 

### How to visualize data
#### Data Basics
The data that you input are the first ingredient to an effective visualization. 

  - Three types of (structured) data: 
    - Tabular
    - Spatial 
    - Network 
  - Data attributes
    - Numerical 
    - Categorical (ordered or unordered)

#### Visualization Grammar: The building blocks of any visualization 
When we create a visualization, we map data values to visual properties. The basic grammar of a visualization is that of marks and channels. `Vega-Altair` relies heavily on this grammar to build out visualizations, so it is useful to cover here. 
  - **Marks**: visual objects that represent data items
    - e.g., point, line
  - **Channels**: *retinal variables* that map particular data attribute to marks that alter their appearance
    - e.g., Position, length, **color**
    - In Vega-Altair, these channels are built directly into chart specifications, including (complete set of options at [Vega-Altair Channels](https://altair-viz.github.io/user_guide/encodings/channels.html#channels)):
      - `x`: Horizontal (x-axis) position of the mark.
      - `y`: Vertical (y-axis) position of the mark. 
      - `size`: Size of the mark. May correspond to area or length, depending on the mark type. 
      - `color`: Mark color, specified as a [legal CSS color](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value). 
      - `opacity`: Mark opacity, ranging from 0 (fully transparent) to 1 (fully opaque). 
      - `shape`: Plotting symbol shape for point marks.
    - Other useful parameters for chart specification include: 
      - `tooltip`: Tooltip text to display upon mouse hover over the mark. 
      - `order`: Mark ordering, determines line/area point order and drawing order. 
      - `column`: Facet the data into horizontally-aligned subplots. 
      - `row`: Facet the data into vertically-aligned subplots.

#### Building the right charts from our grammar of visualization    
- Questions and (some) optimal visualization choices to answer these questions?
  - Magnitude: Compare amounts/sizes. Important to have amount of change in data represented equally by change in visualization 
    - example: bar chart 
  - Ranking: Quickly identify max or min values of a variable
    - example: ordered bar (or column) chart
  - Deviation: Understand variation from a fixed reference point 
    - example: diverging bar
  - Distribution: Show occurrence and spread of values in a dataset
    - example: histogram, box plot/violin plot, strip plot
  - Change over Time: show changing trends over time
    - example: line chart, area chart/streamgraph
  - Correlation: Understand relationship between 2 or more variables (careful to avoid implying causality)
    - example: scatterplot, heatmap, parallel coordinates
  - Part-to-whole: show proportions within a category of data 
    - example: pie/donut chart, stacked bar chart, treemap
  - Spatial: understand spatial patterns and relationships 
    - example: chloropleth map, symbol map
  - Network/Flow: understand connectedness/strengths of relationships
    - example: node-link diagram, adjacency matrix, sankey diagram

#### Interact with your data
  - Manipulate (change, select, navigate)
  - Facet (juxtapose, partition, superimpose)
  - Reduce (filter, aggregate)

### Hands-On: Creating Basic, Interactive Charts with MatPlotLib and Vega-Altair
Now that we have an overview, we'll move into how to create a set of basic and commonly-used charts using two popular visualization libraries in Python: [`vega-altair`](https://altair-viz.github.io/) and [`matplotlib`](https://matplotlib.org/), with a bit of [`folium`](https://python-visualization.github.io/folium/) for spatial data mapping. 

- Basic charts that you will learn how to create:
  - Bar chart (optionally with error bars)
  - Line chart (optionally with 95% CI)
  - Scatterplot
  - Pie chart
  - Faceted charts (e.g. Trellis charts)
  - Chloropleth map 
- Interactive options (much of this is easier to implement in Vega-Altair than in Matplotlib)
  - Tooltips
  - Zoom/Pan
  - Filter
- Good visualization design practices
  - Frame as a conversation
  - Show the data 
  - Break up the information
  - Design with grey
  - Reduce visual clutter
  - Use text to complete your story (some of this is a LOT easier to do *outside* of your python file)
    - Descriptive and active title
    - Remove legends where possible to integrate more closely with the data
    - Add explainers

  
## References and Further Exploration
- Munzner, T., 2014. Visualization analysis and design. CRC press.
- [Observable Data Visualization Course](https://observablehq.com/@observablehq/datavis-course-lesson-1-introduction?collection=@observablehq/data-vis-course), by Robert Kosara 
- [Observable: Data Types, Graphical Marks, and Visual Encoding Channels](https://observablehq.com/@uwdata/data-types-graphical-marks-and-visual-encoding-channels), by Jeff Heer
- [Financial Times Chart Doctor: Visual Vocabulary](https://github.com/Financial-Times/chart-doctor/blob/main/visual-vocabulary/poster.png)
- The Functional Art: An Introduction to Information Graphics and Visualization, Alberto Cairo (New Riders, 2013)
- Building Science Graphics, Jen Christiansen (2023)
- Data Feminism, D'ignazio, Catherine, and Lauren F. Klein (MIT press, 2020). 
- Dear Data, Georgia Lupi & Stephanie Posavec (Penguin Press, 2016)
- Visualization Analysis and Design, Tamara Munzner (CRC Press 2014) (much of the theory content from this presentation comes from this book!)
- History of Information Graphics, Sandra Rendgen (Taschen, 2019)
- The Visual Display of Quantitative Information, Edward Tufte 
- [BBC Style Graphics Cookbook](https://bbc.github.io/rcookbook/#how_to_create_bbc_style_graphics) 


