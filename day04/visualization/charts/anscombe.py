import altair as alt
from vega_datasets import data
import vl_convert as vlc

source = data.anscombe()

chart = alt.Chart(source).mark_circle().encode(
    alt.X('X').scale(zero=False),
    alt.Y('Y').scale(zero=False),
    alt.Facet('Series', columns=4),
).properties(
    width=180,
    height=180,
)

chart.save('anscombe.html')

## write chart to png file
png_data = vlc.vegalite_to_png(chart.to_json(), vl_version="4.17", scale=2)
with open("../docs/anscombe.png", "wb") as f:
    f.write(png_data)
