import altair as alt
from vega_datasets import data
import matplotlib.pyplot as plt
from matplotlib import ticker
import mplcursors

source = data.gapminder_health_income()
print(source.head())

# vega-altair scatterplot
alt_title = alt.TitleParams(
    ["Richest and longest-lived countries", "include all but African region"],
    fontSize=16,
    anchor="start"
)

scatter_plot_log = (
    alt.Chart(source, title=alt_title)
    .mark_circle()
    .encode(
        alt.X("income:Q", scale=alt.Scale(type="log"), title="GDP Per Capita (log scale)", axis=alt.Axis(labelExpr='"$"+datum.value', grid=False)),  # logarithmic scale to spread out countries on lower end of x-scale and encode population into visualization
        alt.Y("health:Q", scale=alt.Scale(zero=False), title="Mortality (years)", axis=alt.Axis(grid=False)),
        size=alt.Size("population:Q", scale=alt.Scale(range=[30, 2000])),  # add size encoding, adjust range param of default
        color=alt.Color(
            "population:Q", legend=None,
            scale=alt.Scale(scheme="tealblues"),  # add color/opacity encoding to regions
        ),
        strokeOpacity=alt.value(1),
        tooltip=["country", "income", "health", "population"]  # add tooltips
    ).configure_view(stroke=None).configure_axisY(
    titleAngle=0,
    titleAlign="left",
    titleY=-15,
    titleX=-40
).interactive()  # add basic interactivity
)
scatter_plot_log.save("alt_scatterplot.html")


# focus color encoding to European region only
# change shapes according to different regions
# facet out data


## Matplotlib version TODO finish
fig, ax = plt.subplots(figsize=(8, 6))

def set_marker_size(x, factor):  # https://datagy.io/matplotlib-marker-size/ -- TODO not happy with this atm
    return [x_i**factor for x_i in x]


area = source["population"]
plt.scatter(source["income"], source["health"], s=set_marker_size(area, 0.35), alpha=0.7, c=source["population"], cmap='GnBu', edgecolors="black")
plt.xscale("log")

plt.title("Richest and Healthiest Countries Include All But African Region", x=0.4, y=1, pad=30)
plt.xlabel("Income ($)")
plt.ylabel("Age", ha="left", y=1.02, rotation=0, labelpad=2)

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
formatter = ticker.ScalarFormatter(useMathText=False)
formatter.set_scientific(False)
ax.xaxis.set_major_formatter(formatter)

# from https://towardsdatascience.com/tooltips-with-pythons-matplotlib-dcd8db758846
cursor = mplcursors.cursor(hover=True)
@cursor.connect("add")
def on_add(sel):
    sel.annotation.set(text=source.iloc[sel.target.index]['country'])


#plt.show()
