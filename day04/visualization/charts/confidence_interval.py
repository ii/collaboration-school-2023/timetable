import altair as alt
import pandas as pd

# CI calculated internally via non-parametric bootstrap of the mean
# see https://acclab.github.io/bootstrap-confidence-intervals.html

source = pd.read_csv("./gapminder.csv")
source["Year"] = pd.to_datetime(source["Year"], format="%Y")
source["Region"] = source["Region"].str.capitalize()
print(source.head())

alt_title = alt.TitleParams(
    ["Increasing but More Varied Global Incomes", "in Recent Years"],
    fontSize=16,
    anchor="start"
)

ci = (
    alt.Chart(source, title=alt_title)
    .mark_errorband(extent="ci", opacity=0.2)
    .encode(
        alt.X("Year:T"),
        alt.Y("Income:Q", axis=alt.Axis(labelExpr='"$"+datum.value'), title="GDP Per Capita"),
    )
)

line = ci.mark_line().encode(alt.Y("mean(Income):Q"))

alt.layer(line, ci).configure_axisY(
    titleAngle=0,
    titleAlign="left",
    titleY=-15,
    titleX=-40
).configure_axis(grid=False).configure_view(
    stroke=None).save("./alt_confidence_interval.html")
