import pandas as pd
import folium

# get data to encode into map 
gapminder = pd.read_csv("gapminder.csv")
gapminder2014 = gapminder.loc[gapminder["Year"]==2014]
gapminder2014["Country Code"] = gapminder2014["Country Code"].str.upper()
print(gapminder2014)

max_income = gapminder2014["Income"].max()
min_income = gapminder2014["Income"].min()
number_of_bins = 6
bin_size = int((max_income - min_income) / number_of_bins)
income_bins = list(range(min_income, max_income + bin_size + 1, bin_size))
print(income_bins)

# create map (following https://realpython.com/python-folium-web-maps-from-data/)
political_countries_url = (
    "http://geojson.xyz/naturalearth-3.3.0/ne_50m_admin_0_countries.geojson" # http://geojson.xyz/
)

map_income = folium.Map(location=(30, 10), zoom_start=3, tiles="cartodb positron")
folium.Choropleth(
    geo_data=political_countries_url,
    data=gapminder2014,
    columns=["Country Code", "Income"],
    key_on="feature.properties.iso_a3",
    #bins=income_bins,
    fill_color="BuPu",  # set to any Brewer color palette (https://en.wikipedia.org/wiki/Cynthia_Brewer#Brewer_palettes)
    fill_opacity=0.8,
    line_opacity=0.3,
    nan_fill_color="grey",
    legend_name="Richest Countries in the World (2014)",
).add_to(map_income)

map_income.save("income.html")



# links and refs
# https://realpython.com/python-folium-web-maps-from-data/ 
# https://dev.to/leul12/geo-coding-country-names-in-python-12ef
