import altair as alt
import pandas as pd
import matplotlib.pyplot as plt

# bring in data
source = pd.read_csv("./gapminder.csv")
source["Region"] = source["Region"].str.capitalize()
source2014 = source.loc[source["Year"] == 2014]
print(source2014.head())

# make bar chart in altair
alt_title = alt.TitleParams(
    "Europeans Earn Highest Average Income in 2014",
    fontSize=16,
    anchor="start"
)

alt_bar = alt.Chart(source2014, title=alt_title, width=400).mark_bar().encode(
    alt.X("Region:N", sort="y", axis=alt.Axis(labelAngle=0)),
    alt.Y("mean(Income):Q", title="Average Annual Income($)")
)
alt_bar.save("alt_bar_err.html")

alt_error = alt.Chart(source2014).mark_errorbar().encode(
    alt.Y("Income:Q").scale(zero=False),
    alt.X("Region:N"),
)

alt_points = alt.Chart(source2014).mark_point(
    filled=True,
    color="black",
).encode(
    alt.Y("mean(Income)"),
    alt.X("Region:N"),
)

alt.layer(alt_bar, alt_error, alt_points).configure_axisY(
    titleAngle=0,
    titleAlign="left",
    titleY=-15,
    titleX=-40
).save("alt_bar_err.html")
########################################################
# make bar chart in matplotlib

# need to first transform data to find mean income for each region
avg_income = source2014.groupby("Region")["Income"].mean().reset_index()
err_income = source2014.groupby("Region")["Income"].sem().reset_index() # calc standard error

fig, ax = plt.subplots(figsize=(8, 6))
ax.grid(which="major", axis="y", color='gray', linestyle='-', linewidth=0.5)
ax.set_axisbelow(True)

ax.bar(avg_income["Region"], avg_income["Income"], width=1, color='xkcd:sky blue', edgecolor="white")
ax.errorbar(avg_income["Region"], avg_income["Income"], yerr=err_income["Income"], fmt="o", color="k", capsize=5)  # add error bars

plt.title("Europeans Earn Highest Average Income in 2014", x=0.23, y=1, pad=24)
plt.xlabel("Region")
plt.ylabel("Average Annual Income($)", ha="left", y=1.02, rotation=0, labelpad=2)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

plt.show()
# plt.savefig('plt_bar.png', dpi=300, bbox_inches='tight')
