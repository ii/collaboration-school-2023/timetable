import altair as alt
import pandas as pd
from altair import datum

source = pd.read_csv("./OWID-energy-consumption.csv")
#print(source.head())

trellis_bar_chart = alt.Chart(source).mark_bar().encode(
    y='Sum of Primary energy consumption per capita (kWh):Q',
    x='Year:O',
    color='Country:N',
    column='Country:N'
)
#trellis_bar_chart.save("trellis-chart.html")

############################################
chart_title = alt.TitleParams(
    ["Norwegians use the Most Energy per Person", "Relative to USA and Germany"],
    subtitle=["For years 2012-2021, with energy use measured in kilowatt-hours", ""],
    fontSize=26,
    subtitleFontSize=16,
    anchor="start"
)

trellis_bar_chart_verbose = alt.Chart(source, title=chart_title, width=300).mark_bar().encode(
    alt.Y('Sum of Primary energy consumption per capita (kWh):Q').title('Per-person energy use (kWh)'),
    alt.X('Year:O', axis=alt.Axis(labelAngle=0)),
    color=alt.Color('Country:N',legend=None).scale(scheme='dark2'),
    column=alt.Column('Country:N', center=False)
).configure_axisY(
    titleAngle=0,
    titleAlign="left",
    titleY=-25,
    titleX=-40,
)
trellis_bar_chart_verbose.save("trellis-chart-verbose.html")

############################################
usa_label = alt.Chart({'values':[{}]}).mark_text(align="left", baseline="top").encode(
    x=alt.value(50),  # pixels from left
    y=alt.value(80),  # pixels from top
    text=alt.value(['USA']))  

germany_label = alt.Chart({'values':[{}]}).mark_text(align="left", baseline="top").encode(
    x=alt.value(50),  # pixels from left
    y=alt.value(160),  # pixels from top
    text=alt.value(['Germany']))  

norway_label = alt.Chart({'values':[{}]}).mark_text(align="left", baseline="top").encode(
    x=alt.value(50),  # pixels from left
    y=alt.value(10),  # pixels from top
    text=alt.value(['Norway']))  

attribute_label = alt.Chart({'values':[{}]}).mark_text(align="left", baseline="top", color="grey").encode(
    x=alt.value(-40),  # pixels from left
    y=alt.value(350),  # pixels from top
    text=alt.value(["Source: Our World in Data (Nov 2021). https://ourworldindata.org/per-capita-energy"])) 

line_chart_verbose = alt.Chart(source, title=chart_title, width=600).mark_line().encode(
    alt.Y('Sum of Primary energy consumption per capita (kWh):Q').title('Per-person energy use (kWh)'),
    alt.X('Year:O', axis=alt.Axis(labelAngle=0)),
    color=alt.Color('Country:N',legend=None).scale(scheme='dark2'),
)

alt.layer(line_chart_verbose,germany_label,usa_label,norway_label,attribute_label).configure_axisY(
    titleAngle=0,
    titleAlign="left",
    titleY=-25,
    titleX=-40,
).configure_view(
    stroke=None).save("energy-line-chart-verbose.html")

################################################################################################################
source_full = pd.read_csv("./OWID-energy-full.csv")
countries = ["Qatar","Singapore", "Iceland","United Arab Emirates","Kuwait","Trinidad and Tobago","Norway","Canada","Oman", "Saudi Arabia"]
source_top10 = source_full.loc[(source_full['year'] == 2018) & (source_full['country'].isin(countries))]

source_top10['Country focus'] = source_top10['country'].where(source_top10['country'] == 'Norway', other='Other')
scale = alt.Scale(domain=['Norway', 'Other'], range=['#d95f0e', 'grey'])
print(source_top10.head())

splom = alt.Chart(source_top10).mark_circle(opacity=0.9).encode(
    alt.X(alt.repeat("column"), type='quantitative'),
    alt.Y(alt.repeat("row"), type='quantitative'),
    color=alt.Color('Country focus', scale=scale),
    tooltip=['country'],
).properties(
    width=150,
    height=150
).repeat(
    row=["population", "gdp", "energy_per_capita", "greenhouse_gas_emissions", "biofuel_elec_per_capita"],
    column=["population", "gdp", "energy_per_capita", "greenhouse_gas_emissions", "biofuel_elec_per_capita"]
)
splom.save("splom-grey-design-with.html")

############################
# normalized parallel coordinates plot 
pcp = alt.Chart(source_top10).transform_window(
    index='count()'
).transform_fold(
    ["population", "gdp", "energy_per_capita", "greenhouse_gas_emissions", "biofuel_elec_per_capita"]
).transform_joinaggregate(
     min='min(value)',
     max='max(value)',
     groupby=['key']
).transform_calculate(
    minmax_value=(datum.value-datum.min)/(datum.max-datum.min),
    mid=(datum.min+datum.max)/2
).mark_line().encode(
    x='key:N',
    y='minmax_value:Q',
    color=alt.Color('Country focus', scale=scale),
    detail='index:N',
    opacity=alt.value(0.5),
    tooltip=["country","population", "gdp", "energy_per_capita", "greenhouse_gas_emissions", "biofuel_elec_per_capita"]
).properties(width=800)
#pcp.save("pcp.html")
