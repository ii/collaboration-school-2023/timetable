import altair as alt
from vega_datasets import data
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

source = pd.read_csv("./gapminder.csv")
source2014 = source.loc[source["Year"] == 2014]
sum_pop_2014 = source2014.groupby("Region")["Population"].sum().reset_index()
sum_pop_2014["Region"] = sum_pop_2014["Region"].str.capitalize()
sum_pop_2014["Percent"] = ((sum_pop_2014["Population"] / sum_pop_2014["Population"].sum()) * 100).map('{:,.1f}%'.format)
print(sum_pop_2014)

## vega-altair version
alt_title = alt.TitleParams(
    "Asian Regions Comprise Over 50% of Global Population (2014)",
    fontSize=16,
)

alt_pie = alt.Chart(sum_pop_2014, title=alt_title).mark_arc(outerRadius=100).encode(
    theta=alt.Theta("Population:Q").stack(True),
    color=alt.Color("Region:N", scale=alt.Scale(scheme="Set2"), legend=None),
    tooltip=[
        #alt.Tooltip("Region:N", title="Region"),
        alt.Tooltip(
            "Percent", title="Region Population"
        ),
    ]
)

slice_breaks = alt_pie.mark_arc(innerRadius=0, stroke="#fff")
text = alt_pie.mark_text(radius=115, size=10, fill="black").encode(text="Region")

alt.layer(alt_pie, slice_breaks, text).resolve_scale(theta="independent").save("alt_pie.html")


#### matplotlib version
colormap = plt.cm.Set2
numberOfSlices = len(sum_pop_2014["Region"])
sliceColors = colormap(np.linspace(0., 1., numberOfSlices))

fig, ax = plt.subplots()
ax.pie(sum_pop_2014["Population"],
       labels=sum_pop_2014["Region"],
       autopct='%1.1f%%',
       colors=sliceColors,
       wedgeprops = {"edgecolor" : "white",
                      'linewidth': 1,
                      'antialiased': True}
       )
plt.title("Asian Regions Comprise Over 50% of Global Population (2014)")
#plt.show()
