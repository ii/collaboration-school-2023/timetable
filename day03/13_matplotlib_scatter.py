import matplotlib.pyplot as plt 
import random as R

xs = [ R.random() for _ in range(100) ]
ys = [ R.random() for _ in range(100) ]
cols = [ R.random() for _ in range(100) ]
sizes = [ 625 * R.random() for _ in range(100) ]

# or, with numpy, efficient for large datasets:
# import numpy as np 
# data = np.random.rand(100,4) # 100 rows of 4 random numbers
# xs = data[:,0]
# ys = data[:,1]
# cols = data[:,2]
# sizes = 256 * data[:,3]

plt.scatter(xs,ys,
            c=cols,
            s=sizes,
            alpha=0.75)

plt.colorbar()
plt.title('100 random points')

plt.savefig('random.png')
