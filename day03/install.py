import sys

libs = ['matplotlib','numpy','pandas', 'altair', 'vega_datasets', 'altair_viewer', 'vl-convert-python',  'folium']

cmd = f"{sys.executable} -m pip install --user {' '.join(libs)}"

ans = input(f"\n\nType 'yes' to try direct install of {libs}: ")
if ans == "yes":
    from subprocess import run
    run(cmd.split())
else:
    print("copy this line into the terminal:\n")
    print(cmd)
    print()
