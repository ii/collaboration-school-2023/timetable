import csv

lats = []
lons = []

with open('akvakultur.csv', newline='', encoding='iso-8859-1') as csvfile:
    akvareader = csv.reader(csvfile, delimiter=';')
    for row in akvareader:
        # errors can be handled in the program with try / except
        # we don't skip the header lines, so the float() conversion fails
        # with ValueError. Instead of stopping, that now gets handled in the except-part
        try:
            lat = float(row[-3]) # latitude is third last
            lon = float(row[-2]) # longitude is second last
        except ValueError:
            continue
        lats.append(lat)
        lons.append(lon)

# can also use try/except around libraries that not everyone has available
try:
    import matplotlib.pyplot as plt
    plt.plot(lons,lats,'+')
    plt.show()
except (ImportError, ModuleNotFoundError) as e:
    print(f'Import of matplotlib failed: {e}')
    print(f'We have {len(lats)} latitudes and {len(lons)} longitudes')
