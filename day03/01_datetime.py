from datetime import date

today = date.today()
my_birthday = date(today.year, 9, 13)

if my_birthday < today:
    my_birthday = my_birthday.replace(year=today.year + 1)

time_to_birthday = my_birthday - today
print(time_to_birthday)


from datetime import datetime

now = datetime.now()
print(now)

exam = datetime(2022, 10, 19, 16, 15, 0)
print(exam)

print(exam - now)
