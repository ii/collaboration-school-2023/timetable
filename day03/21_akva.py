import csv

lats_1 = []
lons_1 = []

lats_2 = []
lons_2 = []

with open('akvakultur.csv', newline='', encoding='utf8') as csvfile:
    akvareader = csv.reader(csvfile, delimiter=';')
    # skip 2 header lines
    next(akvareader)
    next(akvareader)
    for row in akvareader:
        print(row[12])
        lat = float(row[-3]) # latitude is third last
        lon = float(row[-2]) # longitude is second last

    
        if row[12] == "Blåskjell":
            lats_1.append(lat)
            lons_1.append(lon)
   
        if row[12] == "Ørret":
            lats_2.append(lat)
            lons_2.append(lon)


import matplotlib.pyplot as plt
plt.plot(lons_2,lats_2,'*', label="Ørret")
plt.plot(lons_1,lats_1,'+', label="Blåskjell")
plt.legend()
plt.savefig('fisk.png')
plt.show()
