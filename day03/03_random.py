import random

# random.seed(123456) # reproduserbare resultater, ulik for hver int

# randint() simulates a random integer
print(f"A random dice throw: {random.randint(1,6)}")  #  1 <= N <= 6
print()

foods = ["pancakes", "soup", "stir fry", "lasagne"]
# choice() simulates a random choice among a collection of things
print(f"Today's lunch is: {random.choice(foods)}")
# choices() simulates a list of choices
print(f"This week's lunches are: {random.choices(foods, k=7)}")
print()

people = ["Person A", "Person B", "Person C", "Person D", "Person E"]
# sample() simulates a random sample among a collection of things
print(f"The people selected for the trial are: {random.sample(people, k=3)}")
print()


cards = []
for i in range(1, 14):
    cards += [("hearts", i), ("clubs", i), ("spades", i), ("diamonds", i)]

print(f"Unshuffled card deck: {cards[:6]}...")
print()
# shuffle() simulates a random permutation
random.shuffle(cards)
print(f"Shuffled card deck: {cards[:6]}...")
print()
print(f"Player 1 takes the first 3 cards: {cards[:3]}")
print()

# random() without arguments simulates a uniform distribution
# over the interval [0.0, 1.0)
print(f"A random number in the interval [0.0, 1.0): {random.random()}")
print()

# uniform() simulates a uniform distribution over the given interval
print(
    f"A random number in the interval [12, 20]: {random.uniform(12.0, 20.0)}"
)  #  12.0 <= N <= 20.0

# gauss() simulates a normal distribution with the given
# the mean and standard deviation
mu = 40
sigma = 12
print(
    f"A random number from a normal distribution with mean={mu}"
    + f" and standard deviation={sigma}: {random.gauss(mu, sigma)}"
)
