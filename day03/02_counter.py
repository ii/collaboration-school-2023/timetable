from collections import Counter

# text = """Alice was beginning to get very tired of sitting by her sister
#             on the bank, and of having nothing to do: once or twice she had peeped
#             into the book her sister was reading, but it had no pictures
#             or conversations in it, 'and what is the use
#             of a book,' thought Alice 'without pictures or conversation?'"""

# with open('alice.txt') as f:
#     text = f.read()

import urllib.request
with urllib.request.urlopen('https://www.gutenberg.org/files/11/11-0.txt') as f:
    text = f.read().decode('utf8')



letter_count = Counter() # instead of dict() or {}

print(letter_count)

# Count letters
for let in text:
    let = let.lower()
    if let in "abcdefghijklmnopqrstuvwxyz":
        letter_count[let] += 1
# With Counter we don't have to first create a key for every letter

print(letter_count)


for let, count in letter_count.items():
    print(f"{let} is used {count:3d} times")

print('-----')

for let, count in letter_count.most_common(5):
    print(f"{let} is used {count:3d} times")

print("\n\n\n")
exit()

word_count = Counter()

for word in text.split():
    word = word.lower()
    word_count[word] += 1
# With Counter we don't have to first create a key for every word

# The 5 most used words, Counter has a useful helper function most_common()
# we don't need to sort ourselves
print("The 5 most common words")
print(word_count.most_common(10))
for w, c in word_count.most_common(5):
    print(f"{w:14} is used {c:3d} times")
