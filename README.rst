2023 Collaborative programming school
=====================================

In this repository, we'll include all relevant material, it will be updated frequently during the school.

Timetable
---------

This is a rough day-by-day plan, but we may switch up some items at short notice.

Other than the first and last days, doors will open at 8:45 so we can start on time at 9:00. Lunch is at 12:00 each day, and we finish around 16:30. 

----

Day 1 - Monday 19 June
......................

* from 10:00 - Registration
* 10:30 - Welcome and introduction
* Installation of tools: https://inf100.ii.uib.no/notat/installere/
* Windows users also: https://gitforwindows.org/
* 13:00 - Introduction to the command line: https://swcarpentry.github.io/shell-novice/



Day 2 - Tuesday 20 June
.......................

* 09:00 - `Python intro <day02/2023-collab-Py-intro.pdf>`_
* 13:00 - Real-life examples: Jill Walker Rettberg - Machine Vision & GPT stories
* 13:30 - Version control with Git - `part 1: individual use <day02/2023-collab-Git-part_1.pdf>`_

Day 3 - Wednesday 21 June
.........................

* 09:00 - `Python intro part 2 <day03/Python-part-2.pdf>`_, `Program design, objects <day03/OO-Design.pdf>`_
* 11:15 - Korbinian Bösl - `Copyright and licensing <day03/2023-collab_Licensing.pdf>`_ 
* 13:00 - Real-life examples: Ali Farnudi - Wobbly membranes
* 13:30 - Exercises: objects / 'fetch -> analyse -> present' workflow with various datasets
* 18:00 - Dinner at Dr. Wiesener

Day 4 - Thursday 22 June
........................

* 09:00 - Laura Garrison - Visualization: background and python tools
* 13:00 - Real-life examples: Rune Kyrkjebø - CLARIN
* 13:30 - Vis Exercises 
* 14:45 - Git part 2: branches / merging

Day 5 - Friday 23 June
......................

* 09:00 - Useful libraries - finding and using them
* 10:30 - Testing / debugging / (profiling)
* 13:00 - Real-life examples: Henrik Askjer / Jan Ole Bangen - The Language Collections
* 13:30 - Git part 3 - conversational development + exercises

----

Day 6 - Monday 26 June
......................

* 09:00 - Gry Ane Vikanes Lavik - Team collaboration - part 1
* 10:00 - Presentation of project tasks, choose project
* 10:45 - Team collaboration - part 2
* 13:00 - Start project work

Day 7 - Tuesday 27 June
.......................

* 09:00 - `Clean code <day07/2023-collab-Clean-code.pdf>`_, `Documentation and doc tools <day07/2023-collab-docstrings.pdf>`_ 
* 10:45 - Jenny Ostrop - `Open data, FAIR principles <day07>`_
* 13:00 - Project work

Day 8 - Wednesday 28 June
.........................

* 09:00 - `Mixing languages <day08/Mixing.pdf>`_, How are data types represented? unicode, floating point numbers, integers
* 10:45 - Testing and continuous integration (practical)
* 13:00 - Project work

Day 9 - Thursday 29 June
........................

* 09:00 - Short LaTeX intro / Problem solving demo / code review
* 13:00 - Project work

Day 10 - Friday 30 June
.......................

* 09:00 - Finalise presentations
* 10:00 - `Project presentations <presentations/>`_
* 12:30 - End

----

Projects
--------

* `Folk tale motifs <projects/folktales/>`_ --- `Group A repo <https://git.app.uib.no/ii/collaboration-school-2023/folktales-a>`_, `Group B repo <https://git.app.uib.no/ii/collaboration-school-2023/folktales-b>`_
* `Semantle <projects/semantle/>`_ --- `Group repo <https://git.app.uib.no/ii/collaboration-school-2023/semantle>`_ 
* `Story locations <projects/storylocations/>`_ --- `Group repo <https://git.app.uib.no/ii/collaboration-school-2023/story-locations>`_
* `Traffic Jam <projects/trafficjam/>`_

Resources
---------

A `collection of useful links <resources.rst>`_ related to the school's topics.
