LaTeX demo
==========

Unlike the Python world, 
LaTeX tutorial and documentation culture is @!%#$@! bad. 
One page I (DG) found that looks reasonable at first glance 
is https://www.learnlatex.org/, 
the first few chapters look very good, 
but I haven't used it in more detail.


Installation
------------

I'd recommend the https://tug.org/texlive/ distribution, 
which is available for all operating systems. 
Follow the links for installation instructions.


The basics
----------

LaTeX is not a word processor. It is a typesetting system that 
allows you to separate content from formatting. Your job is to 
provide the content and describe its structure. LaTeX takes care of 
the typesetting part.

Like sphinx can turn .rst-files into html pages, LaTeX turns .tex files into PDFs. The main command-line tool is ``pdflatex``, there are more tools for
specialized usecases, such as ``biber`` for bibliography handling, etc...


Live demo files
---------------

The file from the school is in the same repo folder 
as this README file. Use it on the 
command line with ``pdflatex latexexample.tex``

After that, follow https://www.learnlatex.org/ for more. Also try searching 
"LaTeX for linguists" etc. in your favourite search engine.

The LaTeX package for presentations is called ``beamer``: https://latex-beamer.com/quick-start/

