from aoc2017_01_1 import captcha_parser


def test_captcha_junk_input():
    assert 4 == captcha_parser('1111lkjasdfhlkasdjvnhkjagwd!@#R%@#$%W1')


def test_captcha_single_digit():
    assert 0 == captcha_parser("5")


def test_captcha_zero():
    assert 0 == captcha_parser("0")


def test_captcha_empty():
    assert 0 == captcha_parser("")


def test_captcha():
    examples = [1122, 1111, 1234, 9121212129]
    answers = [3, 4, 0, 9]
    for example, answer in zip(examples, answers):
        assert answer == captcha_parser(str(example))


