"""Advent of code 2017, day 01, part 01 solver.

For more info visit
url: https://adventofcode.com/2017/day/1
"""

def main():
    file_name = "input.txt"
    line = read_first_line(file_name)
    answer = captcha_parser(line)
    print(answer)


def read_first_line(file_name):
    """
    Read first line of text file.
    
    Read ONLY the first line of a text file.
    Strip the line of blank space in the beginning and end.
    

    Parameters
    ----------
    file_name: str
        input text file name

    Returns
    -------
    str
        first line of the file.
    """
    with open(file_name, "r") as reader:
        line = reader.readline()
        line = line.strip()
    return line


def captcha_parser(digits):
    length = len(digits)
    if length in [0, 1]:
        return 0

    result = 0
    for i in range(length):
        if digits[i - 1] == digits[i]:
            result += int(digits[i])
    return result


if __name__ == "__main__":
    main()
