#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

#print(sys.argv)


print("\nNumber of arguments:, {}".format(len(sys.argv)))
print("Argument list:\n", end="")

for arg in sys.argv:
    print(arg)
print("\n")
